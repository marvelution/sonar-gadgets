/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.gadgets.sonar.servlet;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.sonar.wsclient.Host;

/**
 * Testcase for {@link SonarMakeRequestServlet}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.8.0
 */
public class SonarMakeRequestServletTest {

	private SonarMakeRequestServlet servlet;

	@Before
	public void before() {
		 servlet = new SonarMakeRequestServlet();
	}

	@Test
	public void testGetHost() {
		Host host = servlet.getHost(URI.create("http://localhost:9000"));
		assertThat(host.getHost(), is("http://localhost:9000"));
		assertThat(host.getUsername(), nullValue());
		assertThat(host.getPassword(), nullValue());
	}

	@Test
	public void testGetHostSecured() {
		Host host = servlet.getHost(URI.create("http://user:user@localhost:9000"));
		assertThat(host.getHost(), is("http://localhost:9000"));
		assertThat(host.getUsername(), is("user"));
		assertThat(host.getPassword(), is("user"));
		
		host = servlet.getHost(URI.create("http://admin:@dmin@localhost:9000"));
		assertThat(host.getHost(), is("http://localhost:9000"));
		assertThat(host.getUsername(), is("admin"));
		assertThat(host.getPassword(), is("@dmin"));
	}

	@Test
	public void testGetHostNoPort() {
		Host host = servlet.getHost(URI.create("http://localhost"));
		assertThat(host.getHost(), is("http://localhost"));
		assertThat(host.getUsername(), nullValue());
		assertThat(host.getPassword(), nullValue());
	}

	@Test
	public void testGetHostNoPortSecured() {
		Host host = servlet.getHost(URI.create("http://user:user@localhost"));
		assertThat(host.getHost(), is("http://localhost"));
		assertThat(host.getUsername(), is("user"));
		assertThat(host.getPassword(), is("user"));
		
		host = servlet.getHost(URI.create("http://admin:@dmin@localhost"));
		assertThat(host.getHost(), is("http://localhost"));
		assertThat(host.getUsername(), is("admin"));
		assertThat(host.getPassword(), is("@dmin"));
	}

	@Test
	public void testGetHostNoScheme() {
		Host host = servlet.getHost(URI.create("localhost:9000"));
		assertThat(host.getHost(), is("http://localhost:9000"));
		assertThat(host.getUsername(), nullValue());
		assertThat(host.getPassword(), nullValue());
	}

	@Test
	public void testGetHostNoSchemeSecured() {
		Host host = servlet.getHost(URI.create("user:user@localhost:9000"));
		assertThat(host.getHost(), is("http://localhost:9000"));
		assertThat(host.getUsername(), is("user"));
		assertThat(host.getPassword(), is("user"));
		
		host = servlet.getHost(URI.create("admin:@dmin@localhost:9000"));
		assertThat(host.getHost(), is("http://localhost:9000"));
		assertThat(host.getUsername(), is("admin"));
		assertThat(host.getPassword(), is("@dmin"));
	}

}
