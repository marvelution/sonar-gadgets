/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.gadgets.sonar.wsclient.connectors;

import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.sonar.wsclient.Host;
import org.sonar.wsclient.connectors.ConnectionException;
import org.sonar.wsclient.connectors.Connector;
import org.sonar.wsclient.services.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Custom {@link Connector} to support force authentication setting in Sonar
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.9.0
 */
public class BasicAuthenticationHttpClient4Connector extends Connector {

	private final Logger logger = Logger.getLogger(BasicAuthenticationHttpClient4Connector.class);
	private final Host host;

	/**
	 * Constructor
	 *
	 * @param host the {@link Host} to create the connector for
	 */
	public BasicAuthenticationHttpClient4Connector(Host host) {
		this.host = host;
	}

	@Override
	public String execute(Query<?> query) {
		HttpGet get = new HttpGet(host.getHost() + query.getUrl());
		initRequest(get, query);
		return executeRequest(get);
	}

	@Override
	public String execute(CreateQuery<?> query) {
		HttpPost post = new HttpPost(host.getHost() + query.getUrl());
		initRequest(post, query);
		setRequestEntity(post, query);
		return executeRequest(post);
	}

	@Override
	public String execute(DeleteQuery query) {
		HttpDelete delete = new HttpDelete(host.getHost() + query.getUrl());
		initRequest(delete, query);
		return executeRequest(delete);
	}

	@Override
	public String execute(UpdateQuery<?> query) {
		HttpPut put = new HttpPut(host.getHost() + query.getUrl());
		initRequest(put, query);
		setRequestEntity(put, query);
		return executeRequest(put);
	}

	/**
	 * Execute the given request
	 *
	 * @param request the {@link HttpRequestBase} request to execute
	 * @return the resulting JSON {@link String}, can be {@code null}
	 */
	protected String executeRequest(HttpRequestBase request) {
		String json = null;
		DefaultHttpClient client = createClient();
		if (logger.isDebugEnabled()) {
			logger.debug("Executing request " + request.getURI() + " on Host");
			logger.debug(" - host: " + host.getHost());
			logger.debug(" - username: " + host.getUsername());
			logger.debug(" - password: " + host.getPassword());
			for (Header header : request.getAllHeaders()) {
				logger.debug("Header: " + header.getName() + ": " + header.getValue());
			}
		}
		try {
			HttpResponse response = client.execute(request, new BasicHttpContext());
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					json = EntityUtils.toString(entity);
				} else if (response.getStatusLine().getStatusCode() != HttpStatus.SC_NOT_FOUND) {
					throw new ConnectionException("HTTP error: " + response.getStatusLine().getStatusCode()
							+ ", msg: " + response.getStatusLine().getReasonPhrase()
							+ ", query: " + request.toString());
				}
			}
		} catch (IOException e) {
			throw new ConnectionException("Query: " + request.getURI(), e);
		} finally {
			client.getConnectionManager().shutdown();
		}
		return json;
	}

	/**
	 * Create a {@link DefaultHttpClient} to use for the http communication
	 *
	 * @return the {@link DefaultHttpClient}
	 */
	protected DefaultHttpClient createClient() {
		DefaultHttpClient client = new DefaultHttpClient();
		HttpParams params = client.getParams();
		HttpConnectionParams.setConnectionTimeout(params, AbstractQuery.DEFAULT_TIMEOUT_MILLISECONDS);
		HttpConnectionParams.setSoTimeout(params, AbstractQuery.DEFAULT_TIMEOUT_MILLISECONDS);
		return client;
	}

	/**
	 * Setter for the Request Entity for the Request given
	 *
	 * @param request the request to set the entity for
	 * @param query   the query to get the entity from
	 */
	protected void setRequestEntity(HttpEntityEnclosingRequestBase request, AbstractQuery<?> query) {
		if (query.getBody() != null) {
			try {
				request.setEntity(new StringEntity(query.getBody(), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				throw new ConnectionException("Encoding is not supported", e);
			}
		}
	}

	/**
	 * Initialize the given request with the comment headers and parameters
	 *
	 * @param request the {@link HttpRequestBase} request to initialize
	 * @param query   the query to get request settings/parameters from
	 */
	protected void initRequest(HttpRequestBase request, AbstractQuery query) {
		request.setHeader("Accept", "application/json");
		if (StringUtils.isNotBlank(host.getUsername())) {
			UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(host.getUsername(),
					host.getPassword());
			try {
				request.setHeader(new BasicScheme().authenticate(credentials, request));
			} catch (AuthenticationException e) {
				throw new ConnectionException("Failed to add Authorization header", e);
			}
		}
		if (query.getLocale() != null) {
			request.setHeader("Accept-Language", query.getLocale());
		}
		request.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, query.getTimeoutMilliseconds());
		request.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, query.getTimeoutMilliseconds());
	}

}
