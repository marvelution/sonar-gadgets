/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.gadgets.sonar.wsclient.utils;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sonar.wsclient.services.Metric;

import com.google.common.collect.Lists;

/**
 * Utility class for {@link Metric} objects
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.5.1
 */
public class MetricUtils {

	public static final String INT_TYPE = "INT";
	public static final String FLOAT_TYPE = "FLOAT";
	public static final String MILLISEC_TYPE = "MILLISEC";
	public static final String PERCENT_TYPE = "PERCENT";
	public static final String RATING_TYPE = "RATING";
	public static final String LEVEL_TYPE = "LEVEL";

	public static final List<String> COLOR_METRIC_TYPES = Lists.newArrayList(LEVEL_TYPE, PERCENT_TYPE);
	public static final List<String> SIZE_METRIC_TYPES = Lists.newArrayList(INT_TYPE, FLOAT_TYPE, MILLISEC_TYPE,
		PERCENT_TYPE);
	public static final List<String> TIMEMACHINE_METRIC_TYPES = Lists.newArrayList(INT_TYPE, FLOAT_TYPE,
		PERCENT_TYPE, MILLISEC_TYPE, RATING_TYPE);

	/**
	 * Check if the given {@link Metric} is a color metric
	 * 
	 * @param metric the {@link Metric} to check
	 * @return <code>true</code> if it is a color {@link Metric}, <code>false</code> otherwise
	 */
	public static boolean isColorMetric(Metric metric) {
		return COLOR_METRIC_TYPES.contains(metric.getType().toUpperCase());
	}

	/**
	 * Check if the given {@link Metric} is a size metric
	 * 
	 * @param metric the {@link Metric} to check
	 * @return <code>true</code> if it is a size {@link Metric}, <code>false</code> otherwise
	 */
	public static boolean isSizeMetric(Metric metric) {
		return SIZE_METRIC_TYPES.contains(metric.getType().toUpperCase())
			&& StringUtils.isNotBlank(metric.getDomain());
	}

	/**
	 * Check if the given {@link Metric} is a time machine metric
	 * 
	 * @param metric the {@link Metric} to check
	 * @return <code>true</code> if it is a time machine {@link Metric}, <code>false</code> otherwise
	 */
	public static boolean isTimeMachineMetric(Metric metric) {
		return TIMEMACHINE_METRIC_TYPES.contains(metric.getType().toUpperCase());
	}

}
