/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.gadgets.sonar.servlet;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import com.marvelution.gadgets.sonar.wsclient.connectors.BasicAuthenticationHttpClient4Connector;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.sonar.wsclient.Host;
import org.sonar.wsclient.Sonar;
import org.sonar.wsclient.connectors.ConnectionException;

import com.marvelution.gadgets.sonar.servlet.query.QueryWrapper;

/**
 * {@link HttpServlet} to handle secured requests to Sonar
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 1.4.0
 */
public class SonarMakeRequestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(SonarMakeRequestServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String requestUrl = req.getParameter("url");
		String requestType = req.getParameter("type");
		if (StringUtils.isNotBlank(requestUrl)) {
			logger.debug("Got a makeRequest request for " + requestUrl);
			try {
				URI uri = new URI(requestUrl);
				Sonar sonar = new Sonar(new BasicAuthenticationHttpClient4Connector(getSonarHostFromRequest(req)));
				String json = sonar.getConnector().execute(new QueryWrapper(uri, requestType));
				resp.setCharacterEncoding("UTF-8");
				if (StringUtils.isBlank(requestType) || "json".equalsIgnoreCase(requestType)) {
					resp.setContentType(MediaType.APPLICATION_JSON);
				} else {
					resp.setContentType(MediaType.APPLICATION_XML);
				}
				if (StringUtils.isNotEmpty(json)) {
					logger.debug("Writing json to response: " + json);
					resp.getWriter().write(json);
					resp.getWriter().flush();
				} else {
					logger.error("Query '" + uri + "' didn't result in any data returning: "
						+ HttpStatus.SC_NO_CONTENT);
					resp.sendError(HttpStatus.SC_NO_CONTENT, "Query '" + uri + "' didn't return anything");
				}
			} catch (ConnectionException e) {
				throw new ServletException("Failed to execute query [" + requestUrl
					+ "]. Please verify the correctness of the query and " + "that the Sonar server is up.", e);
			} catch (URISyntaxException e) {
				throw new ServletException("Invalid query url specified [" + requestUrl + "]", e);
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	/**
	 * Get a Sonar {@link Host} instance from the {@link HttpServletRequest}
	 *
	 * @param req the {@link HttpServletRequest} containing information for the Sonar client
	 * @return the Sonar {@link Host}
	 * @throws URISyntaxException in case of URI issues
	 */
	protected Host getSonarHostFromRequest(HttpServletRequest req) throws URISyntaxException {
		return getHost(new URI(req.getParameter("url")));
	}

	/**
	 * Get the {@link Host} from a given {@link URI}
	 * 
	 * @param uri the {@link URI}
	 * @return the {@link Host}
	 */
	/* package */ Host getHost(URI uri) {
		logger.debug("Parsing " + uri + " to a Sonar Host");
		StringBuilder hostUri = new StringBuilder();
		String authority;
		if (StringUtils.isNotBlank(uri.getAuthority())) {
			hostUri.append(uri.getScheme());
			authority = uri.getAuthority();
		} else {
			hostUri.append("http");
			authority = uri.toString();
		}
		hostUri.append("://");
		if (authority.contains("@")) {
			hostUri.append(authority.substring(authority.lastIndexOf("@") + 1));
		} else {
			hostUri.append(authority);
		}
		Host host = new Host(hostUri.toString());
		if (authority.contains("@")) {
			String userInfo = authority.substring(0, authority.lastIndexOf("@"));
			host.setUsername(userInfo.substring(0, userInfo.indexOf(":")));
			host.setPassword(userInfo.substring(userInfo.indexOf(":") + 1));
		}
		logger.debug("Parsed " + uri + " to Sonar Host {host=" + host.getHost() + ", username=" + host.getUsername()
			+ ", password=" + host.getPassword() + "}");
		return host;
	}

}
