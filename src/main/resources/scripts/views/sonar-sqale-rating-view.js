/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.views.sqale.rating");

AJS.sonar.views.sqale.rating.VIEW_NAME = "sqale.rating";

AJS.sonar.views.sqale.rating.METRICS = 'sqale_rating,sqale_index,ncloc';

/**
 * Generate the Sonar SQALE Rating view
 * 
 * @param baseUrl the base url of the system displaying the view
 * @param server the Sonar server object
 * @param measureData the measure data of a project on Sonar
 * @param metricsDetails the details of the coverage measures
 * @return the jQuery wrapped view object
 */
AJS.sonar.views.sqale.rating.generateView = function(baseUrl, server, measureData, metricsDetails) {
	AJS.sonar.text.load(baseUrl);
	var view = AJS.sonar.views.createViewContainer();
	var ratingMsr = AJS.sonar.utils.getMeasureFromResource(measureData, "sqale_rating");
	AJS.sonar.views.sqale.rating.createDiv("sqale_rating").append(
		AJS.sonar.views.createMeasureRow(server.host, AJS.sonar.views.sqale.rating.VIEW_NAME, measureData.id,
			ratingMsr, AJS.sonar.utils.getMetricFromMetricsArray(metricsDetails, "sqale_rating"), true)
			.addClass("sqale-color").addClass(ratingMsr.frmt_val)
	).appendTo(view);
	AJS.sonar.views.sqale.rating.createDiv("sqale_index").append(
		AJS.sonar.views.createMeasureRow(server.host, AJS.sonar.views.sqale.rating.VIEW_NAME, measureData.id,
			AJS.sonar.utils.getMeasureFromResource(measureData, "sqale_index"),
			AJS.sonar.utils.getMetricFromMetricsArray(metricsDetails, "sqale_index"), true)
	).appendTo(view);
	AJS.sonar.views.sqale.rating.createDiv("ncloc").append(
		AJS.sonar.views.createMeasureRow(server.host, AJS.sonar.views.sqale.rating.VIEW_NAME, measureData.id,
			AJS.sonar.utils.getMeasureFromResource(measureData, "ncloc"),
			AJS.sonar.utils.getMetricFromMetricsArray(metricsDetails, "ncloc"), true)
	).appendTo(view);
	AJS.sonar.views.addViewFooter(view, server.host);
	return view;
}

/**
 * Create a div
 * 
 * @returns the div
 */
AJS.sonar.views.sqale.rating.createDiv = function(metricKey) {
	return AJS.$("<div/>").addClass("sqale-overview").append(
		AJS.$("<h4/>").text(AJS.sonar.text.getMsg("sonar.views.sqale.rating." + metricKey + ".header"))
	);
}
