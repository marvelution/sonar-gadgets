/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.views.sqale.fdr");

AJS.sonar.views.sqale.fdr.VIEW_NAME = "sqale.fdr";

AJS.sonar.views.sqale.fdr.METRICS = 'sqale_rating,sqale_rating_file_distribution';

/**
 * Generate the Sonar SQALE FDR view
 * 
 * @param baseUrl the base url of the system displaying the view
 * @param server the Sonar server object
 * @param measureData the measure data of a project on Sonar
 * @param metricsDetails the details of the coverage measures
 * @return the jQuery wrapped view object
 */
AJS.sonar.views.sqale.fdr.generateView = function(baseUrl, server, measureData, metricsDetails) {
	AJS.sonar.text.load(baseUrl);
	var view = AJS.sonar.views.createViewContainer();
	var bar = AJS.$("<div/>").attr("id", "fdr-bar").appendTo(view);
	for (var i = 1; i <= 5; i++) {
		var type = AJS.sonar.views.sqale.fdr.idToClass(i);
		AJS.$("<div/>").addClass("fdr-legend").addClass(type).text(type).appendTo(bar);
	}
	AJS.$("<h4/>").text(AJS.sonar.text.getMsg("sonar.views.sqale.fdr.sqale_rating_file_distribution")).appendTo(view);
	var measure = AJS.sonar.utils.getMeasureFromResource(measureData, "sqale_rating_file_distribution");
	var data = {
		total: 0,
		ratings: []
	};
	AJS.$(measure.data.split(";")).each(function(index, item) {
		var metric = item.split("=");
		data.ratings.push(metric);
		data.total += parseInt(metric[1]);
	});
	var table = AJS.$("<table/>").attr("id", "fdr-table");
	var row = AJS.$("<tr/>").appendTo(table);
	AJS.$(data.ratings).each(function(index, item) {
		if (item[1] > 0) {
			var cell = AJS.$("<td>").addClass(AJS.sonar.views.sqale.fdr.idToClass(item[0]));
			cell.css("width", parseInt((item[1] / data.total) * 100) + "%");
			cell.appendTo(row);
		}
	});
	view.append(table);
	AJS.sonar.views.addViewFooter(view, server.host);
	return view;
}

AJS.sonar.views.sqale.fdr.idToClass = function(index) {
	var id = parseInt(index);
	return "ABCDE".substring(id - 1, id);
}
