/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.views.sqale.rrc");

AJS.sonar.views.sqale.rrc.VIEW_NAME = "sqale.rrc";

AJS.sonar.views.sqale.rrc.METRICS = 'sqale_rating,blocker_remediation_cost,critical_remediation_cost,major_remediation_cost,minor_remediation_cost,sqale_index';

/**
 * Generate the Sonar SQALE RRC view
 * 
 * @param baseUrl the base url of the system displaying the view
 * @param server the Sonar server object
 * @param measureData the measure data of a project on Sonar
 * @param metricsDetails the details of the coverage measures
 * @return the jQuery wrapped view object
 */
AJS.sonar.views.sqale.rrc.generateView = function(baseUrl, server, measureData, metricsDetails) {
	AJS.sonar.text.load(baseUrl);
	var view = AJS.sonar.views.createViewContainer();
	var table = AJS.$("<table/>").attr("id", "rrc-table").appendTo(view);
	AJS.$("<tr/>").appendTo(table).append(
		AJS.$("<th/>").attr("colspan", 3).append(
			AJS.$("<h4/>").text(AJS.sonar.text.getMsg("sonar.views.sqale.rrc.header"))
		)
	).append(
		AJS.$("<th/>").addClass("cost").append(
			AJS.sonar.text.getMsg("sonar.views.sqale.rrc.cost")
		).append(" ").append(AJS.sonar.views.sqale.rrc.getLegend("cost"))
	).append(
		AJS.$("<th/>").addClass("total").append(
			AJS.sonar.text.getMsg("sonar.views.sqale.rrc.total")
		).append(" ").append(AJS.sonar.views.sqale.rrc.getLegend("total"))
	);
	var previousCumulatedCost = 0;
	var totalCost = AJS.sonar.utils.getMeasureFromResource(measureData, "sqale_index").val;
	var metrics = AJS.sonar.views.sqale.rrc.METRICS.split(",");
	for (var i = 1; i < metrics.length; i++) {
		var measure = AJS.sonar.utils.getMeasureFromResource(measureData, metrics[i]);
		if (measure.val === undefined) {
			measure.val = 0;
		}
		AJS.sonar.views.sqale.rrc.generateRow(server, measureData.id, measure, totalCost, previousCumulatedCost)
			.appendTo(table);
		previousCumulatedCost = measure.val;
	}
	AJS.sonar.views.addViewFooter(view, server.host);
	return view;
}

/**
 * Generate a single measure row
 * 
 * @param server the Sonar Server object
 * @param resourceId the id of the resource
 * @param measure the measure to display
 * @param totalCost the total cost of the resource
 * @param previousCumulatedCost the cost of the previous measure
 * @returns the row
 */
AJS.sonar.views.sqale.rrc.generateRow = function(server, resourceId, measure, totalCost, previousCumulatedCost) {
	var value = measure.val - previousCumulatedCost;
	var severity = AJS.sonar.views.sqale.rrc.getSeverityFromMetric(measure.key);
	var totalSize = parseInt(AJS.sonar.utils.getPercentage(measure.val, totalCost));
	var valueSize = parseInt(AJS.sonar.utils.getPercentage(value, measure.val));
	var drilldownHref = AJS.sonar.views.evaluateDrilldownHref(server.host, resourceId, measure.key);
	var bar = AJS.$("<td/>").addClass("bar");
	if (measure.val > 0) {
		bar.append(
			AJS.$("<a/>").attr({
				"href": drilldownHref
			}).append(
				AJS.$("<div/>").addClass("outer-bar").css("width", totalSize + "%").append(
					AJS.$("<div/>").addClass("inner-bar").css("width", valueSize + "%")
				)
			)
		);
	}
	return AJS.$("<tr/>").append(
		AJS.$("<td/>").addClass("icon").append(
			AJS.$("<img/>").attr({
				"alt": severity,
				"src": server.host + "/images/priority/" + severity.toUpperCase() + ".gif"
			})
		)
	).append(
		AJS.$("<td/>").addClass("label").text(AJS.sonar.text.getMsg("sonar.views.sqale.rrc." + measure.key))
	).append(
		bar
	).append(
		AJS.$("<td/>").addClass("cost").append(
			AJS.$("<a/>").attr({
				"href": drilldownHref
			}).text(AJS.sonar.utils.roundNumber(value, 1))
		)
	).append(
		AJS.$("<td/>").addClass("total").text(AJS.sonar.utils.roundNumber(measure.val, 1))
	);
}

/**
 * Get the severity for the given metric key
 * 
 * @param metric the metric key
 * @returns the severity
 */
AJS.sonar.views.sqale.rrc.getSeverityFromMetric = function(metric) {
	if (metric === "sqale_index") {
		return "info";
	} else {
		return metric.split("_")[0];
	}
}

/**
 * helper method to get the legend div element
 * 
 * @param type the type of legend (cost/total)
 * @returns the div
 */
AJS.sonar.views.sqale.rrc.getLegend = function(type) {
	return AJS.$("<div/>").addClass("legend-" + type);
}
