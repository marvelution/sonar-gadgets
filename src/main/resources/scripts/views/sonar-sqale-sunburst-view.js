/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.views.sqale.sunburst");

AJS.sonar.views.sqale.sunburst.VIEW_NAME = "sqale.sunburst";

AJS.sonar.views.sqale.sunburst.METRICS = 'sqale_rating';

AJS.sonar.views.sqale.sunburst.DEPTH_OPTIONS = [{key: 1, name: 1}, {key: 2, name: 2}, {key: 3, name: 3}];

var labelType, useGradients, nativeTextSupport, animate;

(function() {
	var ua = navigator.userAgent,
		iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
		typeOfCanvas = typeof HTMLCanvasElement,
		nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
		textSupport = nativeCanvasSupport && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
	labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native': 'HTML';
	nativeTextSupport = labelType == 'Native';
	useGradients = nativeCanvasSupport;
	animate = !(iStuff || !nativeCanvasSupport);
})();

/**
 * Generate the Sonar Sunburst view.
 * Using this method, the Sunburst will have the dimensions of the &lt;body/&gt; for the width and height of the Sunburst.
 * 
 * @see AJS.sonar.views.treemap.generateViewDetailed
 * 
 * @param baseUrl the base url of the system displaying the view
 * @param server the Sonar server object
 * @param measureData the measure data of a project on Sonar
 * @param metricsDetails the details of the coverage measures
 * @return the jQuery wrapped view object
 */
AJS.sonar.views.sqale.sunburst.generateView = function(baseUrl, server, measureData, metricsDetails) {
	var bodyTag = AJS.$("body");
	var dimensions = {width: (bodyTag.width() - 20), height: (bodyTag.width() - 20)};
	return AJS.sonar.views.sqale.sunburst.generateViewDetaile(baseUrl, server, measureData, 3, dimensions, null);
}

/**
 * Generate the Sonar SQALE Sunburst view
 * 
 * @param baseUrl the base url of the system displaying the view
 * @param server the Sonar server object
 * @param measureData the measure data of a project on Sonar
 * @param dimensions the dimensions to create the sunburst in
 * @param onChangeCallback the onChange function to call when the depth setting is changed
 * @return the jQuery wrapped view object
 */
AJS.sonar.views.sqale.sunburst.generateViewDetaile = function(baseUrl, server, measureData, depth, dimensions, onChangeCallback) {
	AJS.sonar.text.load(baseUrl);
	var view = AJS.sonar.views.createViewContainer();
	var header = AJS.$("<div/>").addClass("sunburst-header");
	view.append(header);
	var sunburstContainer = AJS.$("<div/>").attr({id: "sonarSunburst"});
	view.append(sunburstContainer);
	AJS.$("<label/>").attr({"for": "depthSelect"}).text(AJS.sonar.text.getMsg("sonar.views.sqale.sunburst.depth") + ": ").appendTo(header);
	AJS.sonar.utils.createMetricSelectElement("sunburstDepth", "depthSelect", AJS.sonar.views.sqale.sunburst.DEPTH_OPTIONS, depth, function() {
		AJS.$("#sunburst-loading").show();
		sunburstContainer.empty();
		AJS.sonar.views.sqale.populateSunburst(sunburstContainer, server, measureData.key, AJS.$("#depthSelect").val(), dimensions);
		if (onChangeCallback != null) {
			try {
				onChangeCallback(AJS.$("#depthSelect").val());
			} catch(x) {}
		}
	}).appendTo(header);
	AJS.$("<img/>").attr({id: "sunburst-loading", src: WAIT_IMAGE_SRC}).appendTo(header);
	AJS.sonar.views.sqale.populateSunburst(sunburstContainer, server, measureData.key, depth, dimensions);
	AJS.sonar.views.addViewFooter(view, server.host);
	return view;
}

AJS.sonar.views.sqale.populateSunburst = function(sunburstContainer, server, resourceKey, depth, dimensions) {
	AJS.$.ajax(AJS.sonar.accessor.getAjaxOptions(server, "/api/plugins/sqale/sunburst?resource=" + resourceKey + "&format=json&depth=" + depth,
		function(data) {
			AJS.sonar.views.sqale.loadSunburst(sunburstContainer, server, resourceKey, data, depth, dimensions);
		}
	));
}

AJS.sonar.views.sqale.loadSunburst = function(sunburstContainer, server, resourceKey, json, depth, dimensions) {
	dimensions.width = Math.max(dimensions.width, 200);
	sunburstContainer.css({
		"width": dimensions.width + "px",
		"height": dimensions.width + "px",
		"position": "relative",
		"margin-left": "auto",
		"margin-right": "auto"
	});
	var sb = new $jit.Sunburst({
		injectInto: sunburstContainer.attr("id"),
		levelDistance: dimensions.width / (2 + 2 * depth),
		Node: {
			overridable: true,
			type: "multipie"
		},
		Label: {
			type: labelType
		},
		NodeStyles: {
			enable: true,
			type: "Native",
			stylesHover: {
				"color": "#369"
			}
		},
		Tips: {
			enable: true,
			onShow: function(tip, node) {
				AJS.$(tip).empty().append(AJS.$("<div/>").addClass("sunburst-tip-title").text(node.data.n));
				if ("$angularWidth" in node.data) {
					AJS.$(tip).append(AJS.format(AJS.sonar.text.getMsg("sonar.views.sqale.sunburst.cost.tip"), node.data.$angularWidth));
				}
			}
		},
		Events: {
			enable: true,
			onClick: function(node) {
				if (!node) return;
				var href = server.host + "/drilldown/";
				if ("r" in node.data) {
					href += "violations/" + resourceKey + "?rule=" + node.data.r;
				} else {
					href += "measures/" + resourceKey + "?metric=sqale_index&model=SQALE&characteristic_id=" + node.id;
				}
				if (window.parent) {
					window.parent.location.href = href;
				} else {
					window.location.href = href;
				}
			}
		},
		onCreateLabel: function(domElement, node) {
			var labels = sb.config.Label.type, aw = node.getData("angularWidth");
			if (labels === "HTML" && (node._depth < 2 || aw > 2000)) {
				domElement.innerHTML = node.name;
			} else if (labels === "SVG" && (node._depth < 2 || aw > 2000)) {
				domElement.firstChild.appendChild(document.createTextNode(node.name));
			}
		},
		onPlaceLabel: function(domElement, node) {
			var labels = sb.config.Label.type;
			if (labels === "SVG") {
				var fch = domElement.firstChild;
				var style = fch.style;
				style.display = "";
				style.cursor = "pointer";
				style.fontSize = "0.8em";
				fch.setAttribute("fill", "#fff");
			} else if (labels === "HTML") {
				var style = domElement.style;
				style.display = "";
				style.cursor = "pointer";
				style.fontSize = "0.8em";
				style.color = "#ddd";
				var left = parseInt(style.left);
				var w = domElement.offsetWidth;
				style.left = (left - w / 2) + "px";
			}
		}
	});
	sb.loadJSON(json);
	sb.refresh();
	AJS.$("#sunburst-loading").hide();
}
