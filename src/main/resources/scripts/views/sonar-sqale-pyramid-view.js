/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.views.sqale.pyramid");

AJS.sonar.views.sqale.pyramid.VIEW_NAME = "sqale.pyramid";

AJS.sonar.views.sqale.pyramid.METRICS = 'sqale_index';

/**
 * Generate the Sonar SQALE Rating view
 * 
 * @param baseUrl the base url of the system displaying the view
 * @param server the Sonar server object
 * @param measureData the measure data of a project on Sonar
 * @param metricsDetails the details of the coverage measures
 * @param sunburst the sunburst data with a depth of 1
 * @return the jQuery wrapped view object
 */
AJS.sonar.views.sqale.pyramid.generateView = function(baseUrl, server, measureData, metricsDetails, sunburst) {
	AJS.sonar.text.load(baseUrl);
	var view = AJS.sonar.views.createViewContainer();
	var table = AJS.$("<table/>").attr("id", "pyramid-table").appendTo(view);
	AJS.$("<tr/>").appendTo(table).append(
		AJS.$("<th/>").attr("colspan", 2).append(
			AJS.$("<h4/>").text(AJS.sonar.text.getMsg("sonar.views.sqale.pyramid.header"))
		)
	).append(
		AJS.$("<th/>").addClass("cost").append(
			AJS.sonar.text.getMsg("sonar.views.sqale.pyramid.cost")
		).append(" ").append(AJS.sonar.views.sqale.pyramid.getLegend("cost"))
	).append(
		AJS.$("<th/>").addClass("total").append(
			AJS.sonar.text.getMsg("sonar.views.sqale.pyramid.total")
		).append(" ").append(AJS.sonar.views.sqale.pyramid.getLegend("total"))
	);
	var totalCost = AJS.sonar.utils.getMeasureFromResource(measureData, "sqale_index").val;
	var cumulated = totalCost;
	AJS.$(sunburst.children.reverse()).each(function(index, item) {
		console.log(item);
		AJS.sonar.views.sqale.pyramid.generateRow(server, measureData.id, item, totalCost, cumulated)
			.appendTo(table);
		cumulated -= item.data.$angularWidth;
	});
	AJS.sonar.views.addViewFooter(view, server.host);
	return view;
}

/**
 * Generate a single measure row
 * 
 * @param server the Sonar Server object
 * @param resourceId the id of the resource
 * @param characteristic the characteristic to display
 * @param totalCost the total cost of the resource
 * @param cumulated
 * @returns the row
 */
AJS.sonar.views.sqale.pyramid.generateRow = function(server, resourceId, characteristic, totalCost, cumulated) {
	var value = characteristic.data.$angularWidth;
	var totalSize = (totalCost > 0 ? parseInt(AJS.sonar.utils.getPercentage(cumulated, totalCost)) : 0);
	var valueSize = (cumulated > 0 ? parseInt(AJS.sonar.utils.getPercentage(value, cumulated)) : 100);
	var drilldownHref = AJS.sonar.views.evaluateDrilldownHref(server.host, resourceId, "sqale_index")
		+ "&model=SQALE&characteristic=" + characteristic.name.toUpperCase();
	var bar = AJS.$("<td/>").addClass("bar");
	if (value > 0) {
		bar.append(
			AJS.$("<a/>").attr({
				"href": drilldownHref
			}).append(
				AJS.$("<div/>").addClass("outer-bar").css("width", totalSize + "%").append(
					AJS.$("<div/>").addClass("inner-bar").css("width", valueSize + "%")
				)
			)
		);
	}
	return AJS.$("<tr/>").append(
		AJS.$("<td/>").addClass("label").text(AJS.sonar.text.getMsg("sonar.views.sqale.pyramid." + characteristic.name))
	).append(
		bar
	).append(
		AJS.$("<td/>").addClass("cost").append(
			AJS.$("<a/>").attr({
				"href": drilldownHref
			}).text(AJS.sonar.utils.roundNumber(value, 1))
		)
	).append(
		AJS.$("<td/>").addClass("total").text(AJS.sonar.utils.roundNumber(cumulated, 1))
	);
}

/**
 * helper method to get the legend div element
 * 
 * @param type the type of legend (cost/total)
 * @returns the div
 */
AJS.sonar.views.sqale.pyramid.getLegend = function(type) {
	return AJS.$("<div/>").addClass("legend-" + type);
}
