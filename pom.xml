<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Licensed to Marvelution under one or more contributor license
  ~ agreements.  See the NOTICE file distributed with this work
  ~ for additional information regarding copyright ownership.
  ~ Marvelution licenses this file to you under the Apache License,
  ~ Version 2.0 (the "License"); you may not use this file except
  ~ in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~  http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied. See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.marvelution</groupId>
		<artifactId>marvelution</artifactId>
		<version>19</version>
	</parent>
	<groupId>com.marvelution.gadgets</groupId>
	<artifactId>sonar-gadgets</artifactId>
	<packaging>jar</packaging>
	<version>2.0.0-SNAPSHOT</version>

	<name>Sonar Gadgets</name>
	<description>Sonar Gadgets for Atlassian products like JIRA and Bamboo</description>
	<url>http://apidocs.marvelution.com/${project.artifactId}/${project.version}</url>
	<inceptionYear>2009</inceptionYear>
	<organization>
		<name>Marvelution</name>
		<url>http://www.marvelution.com/</url>
	</organization>
	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<scm>
		<connection>scm:git:git@bitbucket.org:marvelution/sonar-gadgets.git</connection>
		<developerConnection>scm:git:git@bitbucket.org:marvelution/sonar-gadgets.git</developerConnection>
		<url>https://bitbucket.org/marvelution/sonar-gadgets</url>
	</scm>
	<issueManagement>
		<system>JIRA</system>
		<url>http://issues.marvelution.com/browse/MARVSONARGADGETS</url>
	</issueManagement>
	<ciManagement>
		<system>Bamboo</system>
		<url>http://builds.marvelution.com/browse/MARVSONARGADGETS</url>
		<notifiers>
			<notifier>
				<type>mail</type>
				<configuration>
					<address>ci@lists.marvelution.com</address>
				</configuration>
			</notifier>
		</notifiers>
	</ciManagement>
	<distributionManagement>
		<site>
			<id>marvelution.website</id>
			<url>
				dav:http://repository.marvelution.com/content/sites/apidocs/${project.artifactId}/${project.version}
			</url>
		</site>
	</distributionManagement>

	<properties>
		<project.jdk.revision>1.6</project.jdk.revision>
		<stagingSiteURL>
			dav:http://repository.marvelution.com/content/sites/apidocs-staging/${project.artifactId}/${project.version}
		</stagingSiteURL>
	</properties>

	<dependencies>
		<dependency>
			<groupId>com.atlassian.plugins.rest</groupId>
			<artifactId>atlassian-rest-common</artifactId>
			<version>1.0.5</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.sal</groupId>
			<artifactId>sal-api</artifactId>
			<version>2.1.beta4</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>2.4</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
			<version>1.1.1</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>servlet-api</artifactId>
			<version>2.5</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.ws.rs</groupId>
			<artifactId>jsr311-api</artifactId>
			<version>1.0</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>2.1</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.15</version>
			<type>jar</type>
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<groupId>com.sun.jdmk</groupId>
					<artifactId>jmxtools</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.sun.jmx</groupId>
					<artifactId>jmxri</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>4.0</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.google.collections</groupId>
			<artifactId>google-collections</artifactId>
			<version>1.0</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.codehaus.sonar</groupId>
			<artifactId>sonar-ws-client</artifactId>
			<version>3.2</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.1</version>
			<type>jar</type>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.10</version>
			<type>jar</type>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.8.5</version>
			<type>jar</type>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>net.alchim31.maven</groupId>
				<artifactId>yuicompressor-maven-plugin</artifactId>
				<version>1.1</version>
				<executions>
					<execution>
						<goals>
							<goal>compress</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<!-- Everything on one line -->
					<linebreakpos>-1</linebreakpos>
					<!-- Don't really care about warning messages. They're fairly useless -->
					<jswarn>false</jswarn>
					<excludes>
						<exclude>**/*.xml</exclude>
						<exclude>**/*-min*</exclude>
						<exclude>**/*.properties</exclude>
					</excludes>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>license-header</id>
			<build>
				<plugins>
					<plugin>
						<inherited>true</inherited>
						<groupId>com.mycila.maven-license-plugin</groupId>
						<artifactId>maven-license-plugin</artifactId>
						<configuration>
							<excludes>
								<exclude>**/jit/**</exclude>
								<exclude>**/jQuery/**</exclude>
								<exclude>**/json/**</exclude>
								<exclude>**/**.TXT</exclude>
								<exclude>**/**.txt</exclude>
								<exclude>**/src/site/**</exclude>
								<exclude>**/src/test/resources/**</exclude>
								<exclude>**/**.psd</exclude>
							</excludes>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

	<developers>
		<developer>
			<id>markrekveld</id>
			<name>Mark Rekveld</name>
			<url>http://www.marvelution.com</url>
			<organization>Marvelution</organization>
			<email>markrekveld@marvelution.com</email>
			<organizationUrl>http://www.marvelution.com</organizationUrl>
			<timezone>+1</timezone>
			<roles>
				<role>Marvelution Member</role>
			</roles>
		</developer>
	</developers>
</project>
