**THIS PROJECT IS OUTDATED AND NO LONGER ACTIVELY WORKED ON**
**THE SUCCESSOR OF THIS PROJECT IS THE [SonarQube Integration for the Atlassian Suite](https://marvelution.atlassian.net/wiki/display/SIFAS) PROJECT**

This project features Sonar Gadgets that are included in other Sonar related projects
More details on <https://marvelution.atlassian.net/wiki/display/MARVSONARGADGETS>

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/MARVSONARGADGETS>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
